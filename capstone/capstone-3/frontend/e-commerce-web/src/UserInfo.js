import React from 'react';

// Creates a context object for sharing data between components
const UserInfo = React.createContext();

// UserProvider allows to share data to UserContext, for components/ pages to use.
export const UserProvider = UserInfo.Provider;

export default UserInfo;