import {Form, Row, Col, Container, Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';
import {useState} from 'react';

import * as React from 'react';
import Box from '@mui/material/Box';
import Alert from '@mui/material/Alert';
import IconButton from '@mui/material/IconButton';
import Collapse from '@mui/material/Collapse';
import Button1 from '@mui/material/Button';
import CloseIcon from '@mui/icons-material/Close';


export default function AddProduct(){

    const [Name, setName] = useState("");
    const [Description, setDescription] = useState("");
    const [Price, setPrice] = useState("");
    const [Stock, setStock] = useState("");
    // const [Image, setImage] = useState(null);
    const token = localStorage.getItem('token')

    const [open, setOpen] = React.useState(false);


    const AddProduct = (e) => {
        e.preventDefault();

        fetch('hhttps://capstone2-hernandez-fg9k.onrender.com/products/createProduct', {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
                Name,
                Description,
                Price,
                Stock
            })
        }).then(res => res.json())
        .then(data => {
            console.log(data)
            
        })

        setName('')
        setDescription('')
        setPrice('')
        setStock('')
        setOpen(true);
    }

    return(
        <>
            <Container className="d-flex w-100 h-100 my-3 py-4 justify-content-center">
                <Row className='w-50'>
                    <Col xs={6} md={8} className='p-5 w-100'>
                    {/* enctype="multipart/form-data" */}
                        <Form onSubmit={(e) => AddProduct(e)}>
                            <h2 className='text-center mb-4'>Create Product</h2>
                            <Collapse in={open}>
                                <Alert
                                action={
                                    <IconButton
                                    aria-label="close"
                                    color="inherit"
                                    size="small"
                                    onClick={() => {
                                        setOpen(false);
                                    }}
                                    >
                                    <CloseIcon fontSize="inherit" />
                                    </IconButton>
                                }
                                sx={{ mb: 2 }}
                                >
                                Add Product Successfully!
                                </Alert>
                            </Collapse>
                            <Form.Group controlId="formBasicProductName" className='mb-3'>
                                <Form.Label>Product Name</Form.Label>
                                <Form.Control
                                type="text"
                                value={Name}
                                onChange={(e) => setName(e.target.value)}
                                placeholder="Enter Product Name"
                                />
                            </Form.Group>

                            <Form.Group controlId="formBasicDescription" className="mb-3">
                                <Form.Label>Description</Form.Label>
                                <Form.Control
                                type="text"
                                value={Description}
                                onChange={(e) => setDescription(e.target.value)}
                                placeholder="Enter Description"
                                />
                            </Form.Group>

                            <Form.Group controlId="formBasicPrice" className="mb-3">
                                <Form.Label>Price</Form.Label>
                                <Form.Control
                                type="number"
                                value={Price}
                                onChange={(e) => setPrice(e.target.value)}
                                placeholder="Enter Price"
                                />
                            </Form.Group>
                            <Form.Group controlId="formBasicStock" className="mb-3">
                                <Form.Label>Stock</Form.Label>
                                <Form.Control
                                type="number"
                                value={Stock}
                                onChange={(e) => setStock(e.target.value)}
                                placeholder="Enter Total Stock"
                                />
                            </Form.Group>

                            {/* <Form.Group controlId="formFile" className="mb-3">
                                <Form.Label>Upload Image</Form.Label>
                                <Form.Control type="file" name="Image"
                                onChange={(e) => setImage(e.target.files[0])} />
                            </Form.Group> */}

                            <Button className="btn btn-primary my-3 w-100" variant="primary" type="submit">
                                Submit
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </>
    )
}