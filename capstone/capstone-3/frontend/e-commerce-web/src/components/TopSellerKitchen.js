import { Container, Image} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';

export default function GamingAccessories(){

    const responsive = {
        superLargeDesktop: {
          // the naming can be any, depends on you.
          breakpoint: { max: 4000, min: 1024 },
          items: 5
        },
        desktop: {
          breakpoint: { max: 1024, min: 800 },
          items: 3
        },
        tablet: {
          breakpoint: { max: 800, min: 464 },
          items: 2
        },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1
        }
    }; 

    return (
        <>
            <Container style={{background: 'white'}} className='my-3 p-3'>
                <h2 className='text-center'>Top Sellers in Kitchen</h2>
                <Carousel responsive={responsive} className='pt-3'>
                    <div className='text-center'>
                        <Image style={{maxWidth: '12rem', maxHeight: '18rem'}} src={require("../images/top-kitchen.webp")} rounded />
                        <p>KNOXHULT Kitchen</p>
                    </div>
                    <div className='text-center'>
                        <Image style={{maxWidth: '12rem', maxHeight: '18rem'}} src={require("../images/top-kitchen7.webp")} rounded />
                        <p>tainless teel Nano tep Dark Grey ink kitchen</p>
                    </div>
                    <div className='text-center'>
                        <Image style={{maxWidth: '12rem', maxHeight: '18rem'}} src={require("../images/top-kitchen3.webp")} rounded />
                        <p>Electrolux 70cm Ultimate Taste</p>
                    </div>
                    <div className='text-center'> 
                        <Image style={{maxWidth: '12rem', maxHeight: '18rem'}} src={require("../images/top-kitchen4.webp")} rounded />
                        <p>San-Yang Kitchen Cabinet</p>
                    </div>
                    <div className='text-center'>
                        <Image style={{maxWidth: '12rem', maxHeight: '18rem'}} src={require("../images/top-kitchen5.webp")} rounded />
                        <p>Thor Kitchen Hrg3080u</p>
                    </div>
                    <div className='text-center'>
                        <Image style={{maxWidth: '12rem', maxHeight: '18rem'}} src={require("../images/top-kitchen6.webp")} rounded />
                        <p>Knife Set, 21 Pieces Chef Knife Set</p>
                    </div>
                </Carousel>
            </Container>
        </>
    )
}