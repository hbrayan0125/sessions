import { Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

export default function BottomPage(){
    return (
        <>
            <Container className="p-3" >
                <Row className='d-flex justify-content-center'>
                    <Col>
                        <h5>About Us</h5>
                        <p>Learn more about our e-commerce store and our mission to provide quality products to our customers.</p>
          
                    </Col>
                    <Col>
                        <h5>Contact Us</h5>
                        <p>If you have any questions or need assistance, feel free to contact our customer support team.</p>
           
                    </Col>
                    <Col>
                        <h5>Quick Links</h5>
                        <ul style={{listStyle: "none", padding: "0px"}}>
                            <li><a style={{textDecoration: "none", color: "black"}} href="#">Home</a></li>
                            <li><a style={{textDecoration: "none", color: "black"}} href="#">About Us</a></li>
                            <li><a style={{textDecoration: "none", color: "black"}} href="#">Contact Us</a></li>
                            <li><a style={{textDecoration: "none", color: "black"}} href="#">Categories</a></li>
                        </ul>
                    </Col>
                </Row>
                <hr />
                <Row className="mt-4 pt-2">
                    <Col>
                        <p>&copy; {new Date().getFullYear()} E-commerce Store Website</p>
                    </Col>
                </Row>
            </Container>
        </>
    )
}