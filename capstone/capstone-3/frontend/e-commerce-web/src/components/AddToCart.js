import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2'; 

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));

export default function AddToCart(props){

    const totalPrice = props.price - 58;
    const id = props.id;
    const token = localStorage.getItem('token')
    const navigate = useNavigate();

    const addOrder = (e) => {
        e.preventDefault();
        

        props.selectProduct(props.id, props.name, props.description, props.price)
        navigate("/userDashboard")
    }

    return(
        <>
            <Box sx={{ flexGrow: 1 }} style={{padding: "0px 0px 270px 0px"}}>
                <h1 className='text-center mt-3 mb-3'>Shopping Cart</h1>
                <Grid container spacing={2}>
                    <Grid item xs={8}>
                        <Item  style={{boxShadow: "none"}}>
                            <div style={{display: "flex", justifyContent: "space-between", alignItems: "center"}} className='mx-3'>
                                <Typography gutterBottom style={{fontWeight: "bold", margin: 0}} variant="subtitle1" color="black" component="div">
                                    Package 1 of 1
                                </Typography>
                            </div>
                            <div className='text-left mx-3 mt-4'>
                            <Typography gutterBottom variant="body2" color="black" className='mb-4' component="div">
                                  Customer Order
                             </Typography>
                            <Grid container spacing={2}>
                                <Grid item xs={12} md={6}>
                                    <Item style={{boxShadow: "none", textAlign: "left"}}>
                                        <Typography gutterBottom variant="body1" color="black" component="div">
                                                {/* Lenovo Legion 9i AI-tuned */} {props.name}
                                        </Typography>
                                        <Typography gutterBottom variant="caption" color="black" component="div">
                                            {/* Intel® Celeron® N4500 Processor (1.10 GHz up to 2.80 GHz), Windows 11 Home 64, 4 GB DDR4-2933MHz (Soldered), Cloud Grey */} {props.description}
                                        </Typography>
                                    </Item>
                                </Grid>
                                <Grid item xs={12} md={3}>
                                    <Item style={{boxShadow: "none"}}>
                                        <Typography gutterBottom variant="h6" color="orange" component="div">
                                            ₱ {props.price}
                                            {/* 50000 */}
                                        </Typography>
                                    </Item>
                                </Grid>
                                <Grid item xs={12} md={3}>
                                    <Item style={{boxShadow: "none"}}>
                                        <Typography gutterBottom variant="body1" component="div">
                                            Quantity: <h6 className='text-black-500'>1</h6>
                                        </Typography>
                                    </Item>
                                </Grid>
                            </Grid>
                            </div>
                        </Item>
                    </Grid>
                    <Grid item xs={4}>
                        <Item style={{boxShadow: "none"}}>
                            <div style={{display: "flex", flexDirection: "column"}} className='my-2 px-2'>
                                <Typography gutterBottom variant="subtitle1" component="div">
                                    Localtion:
                                </Typography>
                                <Typography gutterBottom variant="subtitle1" component="div">
                                    Purok 2 Lanang Candaba Pampanga
                                </Typography>
                            </div>
                            <hr/>
                            <Typography gutterBottom style={{fontWeight: "bold"}} className="mt-3" variant="h6" color="black" component="div">
                                    Order Summary
                            </Typography>

                            <div style={{display: "flex", justifyContent: "space-between"}} className='mt-3 mb-2 px-2'>
                                <Typography gutterBottom variant="subtitle1" component="div">
                                    Subtotal (1 Items)
                                </Typography>
                                <Typography gutterBottom style={{fontWeight: "bold"}} variant="body1" color="black" component="div">
                                    ₱{props.price}
                                    {/* 50000 */}
                                </Typography>
                            </div>
                            <div style={{display: "flex", justifyContent: "space-between"}} className='my-2 px-2'>
                                <Typography gutterBottom variant="subtitle1" component="div">
                                    Shipping Fee
                                </Typography>
                                <Typography gutterBottom style={{fontWeight: "bold"}} variant="body1" color="black" component="div">
                                    ₱58
                                </Typography>
                            </div>
                            <hr />
                            <div style={{display: "flex", justifyContent: "space-between"}} className='my-2 px-2'>
                                <Typography gutterBottom variant="subtitle1" component="div">
                                    Total Price:
                                </Typography>
                                <Typography gutterBottom style={{fontWeight: "bold"}} variant="body1" color="orange" component="div">
                                    ₱{totalPrice}
                                    {/* 50058 */}
                                </Typography>
                            </div>
                            <Button variant="contained" onClick={(e) => {addOrder(e)}} color="warning" className='w-full py-3'>Place Holder Now</Button>
                        </Item>
                    </Grid>
                </Grid>
            </Box>
        </>
    )
}