import {Card, Container, Row, Col} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Link} from 'react-router-dom';

export default function Cards(){
    return (
        <>
            <Container style={{background: 'white'}} className='p-3'>
                <Row>
                    <Col xs={6} md={3} className='d-flex justify-content-center'>
                        <Card style={{ width: '22rem' ,borderRadius: "0px" }} className="me-3">
                            <Card.Body>
                                <Card.Title> Gadget </Card.Title>
                                <Card.Img style={{ borderRadius: "0px", height: "18rem" }} className="my-2" variant="top" src={ require("../images/MacBook Air with M2 (2022).webp")} />
                                <Link style={{textDecoration: 'none'}} variant="primary">See More</Link>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={6} md={3} className='d-flex justify-content-center'>
                        <Card style={{ width: '22rem' ,borderRadius: "0px" }} className="me-3">
                            <Card.Body>
                                <Card.Title> Staff Toy </Card.Title>
                                <Card.Img style={{ borderRadius: "0px", height: "18rem" }} className="my-2" variant="top" src={ require("../images/staff-toy.webp")} />
                                <Link style={{textDecoration: 'none'}} variant="primary">See More</Link>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={6} md={3} className='d-flex justify-content-center'>
                        <Card style={{ width: '22rem' ,borderRadius: "0px" }} className="me-3">
                            <Card.Body>
                                <Card.Title>Home & Kitchen</Card.Title>
                                <Card.Img style={{ borderRadius: "0px", height: "18rem" }} className="my-2" variant="top" src={ require("../images/Plate.jfif")} />
                                <Link style={{textDecoration: 'none'}} variant="primary">See More</Link>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={6} md={3} className='d-flex justify-content-center'>
                        <Card style={{ width: '22rem' ,borderRadius: "0px" }} className="me-3">
                            <Card.Body>
                                <Card.Title> Health & Personal Care</Card.Title>
                                <Card.Img style={{ borderRadius: "0px", height: "18rem" }} className="my-2" variant="top" src={ require("../images/Lotion.jfif")} />
                                <Link style={{textDecoration: 'none'}} variant="primary">See More</Link>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    )
}