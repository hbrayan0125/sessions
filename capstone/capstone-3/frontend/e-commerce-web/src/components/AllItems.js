import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import {useState, useEffect} from 'react';
import Divider from '@mui/material/Divider';
import Checkout from './Checkout';
import AddToCart from './AddToCart';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));


export default function AllItems() {

    const [product, setProduct] = useState([]);
    const [isCheckout, setCheckout] = useState(false);
    const [isAddToCart, setAddToCart] = useState(false);
    const [checkoutComponent, setCheckoutComponent] = useState(null);
    const [CartComponent, setCartComponent] = useState(null);

    const back = (close) => {
        setCheckout(close)
    }

    const backAddTo = (close) => {
        setAddToCart(close)
    }

    const selectProduct = (id, name, description, price) => {
        console.log(id)
        backAddTo(false);
        setCheckout(true);
        setCheckoutComponent( <Checkout Open={back} id={id} name={name} description={description} price={price}/> )
    }

    const selectCart = (id, name, description, price) => {
        console.log(id)
        setAddToCart(true);
        setCartComponent( <AddToCart selectProduct={selectProduct} id={id} name={name} description={description} price={price}/> )
    }
    

    useEffect(()=>{

        fetch(`https://capstone2-hernandez-fg9k.onrender.com/products/retrieveAllActiveProduct`)
        .then(res => res.json())
        .then(data => {

            if(data == false){

                setProduct( 

                    <Grid container spacing={2} className='mt-3' style={{display: "flex", justifyContent: "center"}}>

                            <Typography variant="body2" gutterBottom style={{height: "65vh"}}>
                                                No Products Found
                            </Typography>
                        
                     </Grid>
                );

            }else{
                setProduct(data.map((result) => {

                    const productId = result._id;
    
                    return (
                        <Grid item xs={6} sm={4} md={3} key={result._id} style={{ display: 'flex', flexDirection: 'column', minHeight: '250px' }}>
                            <Item style={{ flex: 1, display: 'flex', flexDirection: 'column', justifyContent: "center" }}>
                                {/* <CardMedia
                                    sx={{ height: 140 }}
                                    image="/static/images/cards/contemplative-reptile.jpg"
                                    title="green iguana"
                                /> */}
                                <Box sx={{ mt: 3, mx: 2 }}>
                                    <Grid container alignItems="center">
                                    <Grid item xs>
                                        <Typography gutterBottom variant="h5" className='text-black-400' component="div">
                                        {result.Name}
                                        </Typography>
                                    </Grid>
                                    </Grid>
                                    {/* <Typography gutterBottom variant="h6" className='text-orange-400' component="div">
                                    ₱{result.Price}
                                    </Typography> */}
                                    <Grid container alignItems="center">
                                    <Grid item xs>
                                        <Typography gutterBottom variant="h6" className='text-orange-400' component="div">
                                        ₱{result.Price}
                                        </Typography>
                                    </Grid>
                                    <Grid item xs>
                                        <Typography gutterBottom variant="h6" className='text-black-400' component="div">
                                        Total: ({result.Stock})
                                        </Typography>
                                    </Grid>
                                    </Grid>
                                    <Typography color="text.secondary" variant="body2">
                                    {result.Description}
                                    </Typography>
                                </Box>
                                <Divider variant="middle" />
                                <Box sx={{ mt: 3, ml: 1, mb: 1 }}>
                                    <Button size="medium" variant="contained" onClick={() => {selectProduct(productId, result.Name, result.Description, result.Price)}} className='me-2' color="warning" >Buy Now</Button>
                                    <Button size="medium" variant="outlined" color="warning"  onClick={() => {selectCart(productId, result.Name, result.Description, result.Price)}}>Add to cart</Button>
                                </Box>
                                
                            </Item>
                        </Grid>
                    )
    
                    })
                );
            }

           

        });

    }, [])

  return (
    <>
        {(isCheckout === true)?
        checkoutComponent
        :
        (isAddToCart === true)?
        CartComponent
        :
        <Box sx={{ width: '100%' }} className='py-5'>
            <h1 className='text-center mb-4 mt-1'>All Product Items</h1>
            <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 3, md: 4 }}>
                {product}
            </Grid>  
        </Box>
        }
    </>
  );
}