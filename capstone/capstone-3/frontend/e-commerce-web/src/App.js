import './App.css';
// Bootstrap
import {Container} from 'react-bootstrap';


// Router, Routes, Route
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom';

// React
import {useState, useEffect} from 'react';

// UserProvider
import {UserProvider} from './UserInfo';

// Pages
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Home from './pages/Home';
import AppNavbar from './components/AppNavbar.js';
import ContactUs from './pages/ContactUs';
import AboutUs from './pages/AboutUs';
import UserDashboard from './pages/UserDashboard';
import AddProduct from './components/AddProduct';
import DeleteProduct from './pages/DeleteProduct';
import AdminNav from './components/AdminNav';
import AdminDashboard from './pages/AdminDashboard';
import UpdateProduct from './pages/UpdateProduct';
import Checkout from './components/Checkout';
import UserOrder from './pages/UserOrder';
import AddToCart from './components/AddToCart';



function App() {
  // Gets the token from local storage and assigns the token to user state
  const [user, setUser] = useState({
    // token: localStorage.getItem('token')
    id: null,
    isAdmin: null,
    Firstname: null,
    Lastname: null,
  });

  // unsetUser is a function for clearing local storage 
  const unsetUser = () => {
    localStorage.clear();
  }

  // Check if user info is properly stored upon login and the localStorage if cleared upon logout

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]);

  return (
    <>
        <div className='body-background-color'>
          <UserProvider value={{user, setUser, unsetUser}}>
            <Router>
            {(user.isAdmin === true)?
                  <AdminNav />
                  :
                  <AppNavbar />
                } 
              <Container>
                <Routes>
                  <Route path="/" element = {<Home />}/>
                  <Route path="/login" element = {<Login />}/>
                  <Route path="/register" element = {<Register />}/>
                  <Route path="/contactus" element = {<ContactUs />}/>
                  <Route path="/aboutus" element = {<AboutUs />}/>
                  <Route path="/userDashboard" element = {<UserDashboard />}/>
                  <Route path="/adminDashboard" element = {<AdminDashboard />}/>
                  <Route path="/logout" element = {<Logout />}/>
                  <Route path="/addProduct" element = {<AddProduct />}/>
                  <Route path="/deleteProduct" element = {<DeleteProduct />}/>
                  <Route path="/updateProduct" element = {<UpdateProduct />}/>
                  <Route path="/checkOut" element = {<Checkout />}/>
                  <Route path="/userOrder" element = {<UserOrder />}/>
                  <Route path="/addToCart" element = {<AddToCart />}/>
                </Routes> 
              </Container>
            </Router>
          </UserProvider>
        </div>
      
    </>
  );
}

export default App;
