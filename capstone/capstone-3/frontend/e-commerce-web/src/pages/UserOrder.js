import * as React from 'react';
import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import ButtonBase from '@mui/material/ButtonBase';
import { useEffect, useState } from 'react';

const Img = styled('img')({
  margin: 'auto',
  display: 'block',
  maxWidth: '100%',
  maxHeight: '100%',
});

export default function UserOrder() {

    const token = localStorage.getItem('token')
    // const [productInfo, setProductInfo] = useState([]);
    const [products, setProducts] = useState([]);


  useEffect(()=>{

    fetch('https://capstone2-hernandez-fg9k.onrender.com/users/order/RetrieveUserOrder', {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            Authorization:  `Bearer ${token}`
            }
        }).then(res => res.json())
        .then(data => {
        // console.log(productData)

        if(data){

            const productsData = data.map((result) => {
                // console.log(result.products.productName)
            //    setProductInfo(result.products)
                // let products = products + result.products
               // dsfasfdasfa
                    return (
                        <>
                        <Grid key={result._id} item xs={12} md={10} sm container className='mb-3 p-3' style={{backgroundColor: "white"}}>
                            <Grid item xs container direction="column" spacing={2}>
                                <Grid item xs>
                                    <Typography gutterBottom variant="subtitle1" component="div">
                                        Product Details
                                    </Typography>
                                    <Typography gutterBottom variant="subtitle1" component="div">
                                        {result.products.productName}
                                    </Typography>
                                    <Typography variant="body2" gutterBottom>
                                        {result.products.productDescription}
                                    </Typography>
                                    {/* <Typography variant="body2" color="text.secondary">
                                        ID: 1030114
                                    </Typography> */}
                                </Grid>
                                {/* <Grid item>
                                <Typography sx={{ cursor: 'pointer' }} variant="body2">
                                    Remove
                                </Typography>
                                </Grid> */}
                            </Grid>
                            <Grid item style={{marginLeft: "1rem"}}>
                                <Typography variant="subtitle1" component="div">
                                Total Price
                                </Typography>
                                <Typography variant="subtitle1" color="orange" component="div">
                                ${result.products.productPrice}
                                </Typography>
                            </Grid>
                        </Grid>
                        </>
                )
                
               
            }) 

            // If there are no products, you can display a message
            const noProductsMessage = data.length === 0 ? (
                <Typography variant="body2" gutterBottom>
                No products found.
                </Typography>
            ) : null;
            // Combine the product data and the message
            setProducts([noProductsMessage, ...productsData]);
        }else{
            const empty = () => {
                return (
                    <Typography variant="body2" gutterBottom style={{height: "78vh"}}>
                        No products found.
                    </Typography>
                )
            }
            setProducts(empty)
            
        }
        
            
        
        
    })
    
  }, [])


  return (
    <>
        <h1 className='text-center mt-4 mb-2'>Order Details</h1>
        <Grid container spacing={2} className='mt-3' style={{display: "flex", justifyContent: "center"}}>
            {/* <Grid item>
            <ButtonBase sx={{ width: 128, height: 128 }}>
                <Img alt="complex" src="/static/images/grid/complex.jpg" />
            </ButtonBase>
            </Grid> */}
            
                    
                {products}
            
        </Grid>
       
        
    </>
  );
}