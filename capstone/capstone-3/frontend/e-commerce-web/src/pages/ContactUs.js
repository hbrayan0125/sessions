import {Form, Button, Image} from 'react-bootstrap';
import '../App.css'

export default function ContactUs() {
  return (
    <>
        <div className='ContactUs'>
            <Image src={require("../images/newbanner.png")} fluid/>
        </div>
        <Form className='mt-3' style={{padding: "1rem 2.5rem 3rem 2.5rem"}}>

            <Form.Group  className="mb-3">
                <Form.Label>Your Name: </Form.Label>
                <Form.Control type="text" placeholder="Enter your Name" />
            </Form.Group>

            <Form.Group  className="mb-3">
                <Form.Label>Email:</Form.Label>
                <Form.Control type="email" placeholder="Enter your Email" />
            </Form.Group>

            <Form.Group  className="mb-3">
                <Form.Label>Phone Number:</Form.Label>
                <Form.Control type="number" placeholder="Enter your Phone Number" />
            </Form.Group>

            <Form.Group  className="mb-3">
                <Form.Label>Company Name:</Form.Label>
                <Form.Control type="text" placeholder="Enter your Company Name" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                <Form.Label>Message:</Form.Label>
                <Form.Control as="textarea" rows={3} />
            </Form.Group> 

            <Button variant="primary" type="submit">
                Submit
            </Button>
        </Form>
    </>
  );
}