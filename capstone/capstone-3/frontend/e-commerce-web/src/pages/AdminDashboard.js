import React, {useState, useEffect} from 'react';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { TableVirtuoso } from 'react-virtuoso';
import DeleteProduct from './DeleteProduct';
import UpdateProduct from './UpdateProduct';



export default function ReactVirtualizedTable() {
  
  const [product, setProduct] = useState([]);

  
  useEffect(() => {
  
    
        fetch(`https://capstone2-hernandez-fg9k.onrender.com/products/retrieveAllProduct`)
        .then(res => res.json())
        .then(data => {

          

          if(data == false){
            console.log(data)
            setProduct(
                
                  [
                    '',
                    '',
                    '',
                    '', 
                    '', 
                    // <>
                    //   <DeleteProduct product_id={product._id}/>
                    //   <UpdateProduct product_id={product._id}/>
                    // </>
                    ''
                  ]
              
            );
          }else{
            setProduct(data.map((product) => {
              
                return (
                  [
                    product.Name,
                    product.Description,
                    product.Price,
                    product.Stock, 
                    product.createdOn, 
                    <>
                      <DeleteProduct product_id={product._id}/>
                      <UpdateProduct product_id={product._id}/>
                    </>
                  ]
                );

              })
            );
          }
  
          
        
        });
  
  }, [])
  
  
  const sample = product;
  
  function createData(id, productname, productdescription, productprice, productstock, productdate, action) {
    return { id, productname, productdescription, productprice, productstock, productdate, action };
  }

  const columns = [
    {
      width: 120,
      label: 'Product Name',
      dataKey: 'productname',
    },
    {
      width: 120,
      label: 'Product Description',
      dataKey: 'productdescription',
      numeric: true,
    },
    {
      width: 120,
      label: 'Product Price',
      dataKey: 'productprice',
      numeric: true,
    },
    {
      width: 120,
      label: 'Product Stock',
      dataKey: 'productstock',
      numeric: true,
    },
    {
      width: 120,
      label: 'Product Date Created',
      dataKey: 'productdate',
      numeric: true,
    },
    {
      width: 120,
      label: 'Action Button',
      dataKey: 'action',
      numeric: true,
    },
  ];
  
  const rows = Array.from({ length: sample.length }, (_, index) => {
    sample.sort((a, b) => a - b);
    // Use the sorted selection for each index
    const sortedSelection = sample[index];
    return createData(index, ...sortedSelection);
  });
  
  const VirtuosoTableComponents = {
    Scroller: React.forwardRef((props, ref) => (
      <TableContainer component={Paper} {...props} ref={ref} />
    )),
    Table: (props) => (
      <Table {...props} sx={{ borderCollapse: 'separate', tableLayout: 'fixed' }} />
    ),
    TableHead,
    TableRow: ({ item: _item, ...props }) => <TableRow {...props} />,
    TableBody: React.forwardRef((props, ref) => <TableBody {...props} ref={ref} />),
  };
  
  function fixedHeaderContent() {
    return (
      <TableRow>
        {columns.map((column) => (
          <TableCell
            key={column.dataKey}
            variant="head"
            align={column.numeric || false ? 'right' : 'left'}
            style={{ width: column.width }}
            sx={{
              backgroundColor: '#0d6efd',
              color: "white"
            }}
          >
            {column.label}
          </TableCell>
        ))}
      </TableRow>
    );
  }
  
  function rowContent(_index, row) {
    return (
      <React.Fragment>
        {columns.map((column) => (
          <TableCell
            key={column.dataKey}
            align={column.numeric || false ? 'right' : 'left'}
          >
            {row[column.dataKey]}
          </TableCell>
        ))}
      </React.Fragment>
    );
  }
  
  return (
    <>
      <div className='py-4'>
        <h1 className="text-center mb-3">View Product</h1>
        <Paper style={{ height: 500, width: '100%' }} className="mb-4">
          <TableVirtuoso
            data={rows}
            components={VirtuosoTableComponents}
            fixedHeaderContent={fixedHeaderContent}
            itemContent={rowContent}
          />
        </Paper>
      </div>
    </>
  );
}