import {Container, Image, Card, Row, Col} from 'react-bootstrap';
import '../App.css'
import {Link} from 'react-router-dom';

export default function ContactUs() {
  return (
    <>
        <div className='AboutUs'>
            <Image src={require("../images/newbanner.png")} fluid/>
        </div>

        <div className='mt-5'>
            <p className="text-center mx-5" style={{fontSize: "1.2rem"}}>
            Welcome to E-Commerce-Website, where our journey began with a desire to make online services easier to access wherever you are. Founded on October 08, 2023, we've since dedicated ourselves to By providing a platform for connecting consumers and sellers within a community, we hope to improve the world by utilizing the possibility of change in technology.
            </p>
        </div>

        <Container className='mt-5'>
            <Row>
                <Col>   
                    <Card className="p-4 text-center" style={{boxShadow: "1px 1px 1px 1px rgba(0, 0, 0, 0.5)", border: "none", cursor: "pointer"}}>
                        <Card.Title>Our Mission</Card.Title>
                        <Card.Body>
                            <Card.Text>
                            Our goal at E-Commerce-Website is to give clients an outstanding online shopping experience that makes it simple for them to find and buy high-quality products. We're dedicated to offering top-notch goods and services, assuring customer happiness through smooth transactions, and having a beneficial influence on our neighborhood by promoting economic development and sustainable lifestyles.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col>
                    <Card className="p-4 text-center" style={{boxShadow: "1px 1px 1px 1px rgba(0, 0, 0, 0.5)", border: "none", cursor: "pointer"}}>
                        <Card.Title>Our Vision</Card.Title>
                        <Card.Body>
                            <Card.Text>
                            At E-Commerce-Website, our vision is simple: to transform the online shopping experience by effortlessly matching customers with the products they want and cultivating a feeling of comfort and joy in every experience. In the future, we see e-commerce overcoming barriers to transportation, bringing people closer to the goods they love and need, and making purchasing a fun and simple experience for everyone.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>

        <Container className="my-4">
            <div>
                <h5>Our Values</h5>
                <p>Our values define who we are and guide every decision we make:</p>
                <ul>
                    <li>
                    <b>Integrity:</b> We uphold the highest standards of honesty, transparency, and ethical behavior in all our actions and decisions. Integrity is the bedrock of our reputation and the trust we build with our stakeholders.
                    </li>
                    <li>
                    <b>Innovation:</b> We foster a culture of continuous innovation, embracing change, and curiosity. We believe that staying at the forefront of industry trends and technologies allows us to provide cutting-edge solutions to our customers.
                    </li>
                    <li>
                    <b>Customer Obsession:</b> Our customers are at the heart of everything we do. We are committed to delivering exceptional value, understanding their needs, and providing the best possible experience. Customer obsession drives us to exceed expectations and build lasting relationships.
                    </li>
                </ul>
            </div>
        </Container>

        <Container className="my-4 pb-5">
            <div>
                <h5>Why Choose Us</h5>
                <ul>
                    <li>
                    Quality: We are committed to delivering top-notch products and services that meet and exceed your expectations.
                    </li>
                    <li>
                    Innovation: By utilizing new technology and creative ideas, we consistently work to be at the forefront of our industry and offer our consumers the latest and most innovative products and services possible.
                    </li>
                    <li>
                    Customer-Centric: Your satisfaction is our priority, and we're always here to listen and improve.
                    </li>
                    <li>
                    Community Engagement: We believe in giving back to our community and making a positive impact.
                    </li>
                </ul>
                <p>
                We appreciate the choice you made to make purchases on our website. We're excited to start this journey with you and look forward to reaching new accomplishments in innovation and success.

                For inquiries or to learn more about us, 
                please <Link style={{textDecoration: "none"}} to={"/contactus"}> Contact Us </Link>.
                </p>
            </div>
        </Container>

        <hr/>
        <footer>
            <Container>
                <Row>
                    <Col md={4}>
                        <h4>About Our Company</h4>
                        <p>We are a passionate team dedicated to providing top-notch services to our clients.</p>
                    </Col>
                    <Col md={4}>
                        <h4>Our Mission</h4>
                        <p style={{margin: "0px"}}>To deliver innovative solutions that meet the needs of our customers.</p>
                        <p>To empower individuals and businesses with cutting-edge technology solutions.</p>
          
                    </Col>
                    <Col md={4}>
                        <h4>Contact Us</h4>
                        <p style={{margin: "0px"}}>Email: hbrayan0125@gmail.com</p>
                        <p>Phone: 09269583966</p>
                    </Col>
                </Row>

                <Row className="mt-2 pt-2">
                    <Col>
                        <p>&copy; {new Date().getFullYear()} E-commerce Store Website</p>
                    </Col>
                </Row>
            </Container>
        </footer>
    
    </>
  );
}