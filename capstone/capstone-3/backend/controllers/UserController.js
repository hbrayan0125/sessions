// [ SECTION ] Dependecies and Modules
const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth.js')

module.exports.registerUser = async (reqBody) => {
  console.log(reqBody);

  try {
      // Check if the email already exists in the database
      const existingUser = await User.findOne({ Email: reqBody.Email });

      if (existingUser) {
          // If the email exists, return an error or handle it as needed
          return { success: false, message: 'Email already exists' };
      } else {
          // Create a new user if the email doesn't exist
          const newUser = new User({
              Email: reqBody.Email,
              Password: bcrypt.hashSync(reqBody.Password, 10),
              Firstname: reqBody.Firstname,
              Lastname: reqBody.Lastname
          });

          const accessToken = auth.createAccessToken(newUser);

          const savedUser = await newUser.save();
          return { success: true, user: savedUser, access: accessToken };
      }
  } catch (error) {
      // Handle any potential errors, e.g., database errors
      return { success: false, message: 'Internal server error' };
  }
}

  // User Authentication
module.exports.UserAuthentication = async (req, res) => {
  
    console.log(req)
    const user = await User.findOne({ Email: req.Email });
    if (!user) {
        // Email not found in the database, return an error response
        return ({ message: false });
    }

    const isPasswordCorrect = bcrypt.compareSync(req.Password, user.Password);

    if (isPasswordCorrect) {
        // Password is correct, generate an access token and send it as a response
        const accessToken = auth.createAccessToken(user);
        // return res.send({ access: accessToken });
        return ({ access: accessToken });
    } else {
        // Password is incorrect, return an error response
        // return res.send({ message: 'Incorrect password' });
        return ({ incorrectPass: true });
    }

    // return User.findOne({ Email: req.Email }).then(result => {
    //   console.log(result);
    //   if(result === null){
    //     //return res.send(false); // the email doesn't exist in our DB
    //     return false; // the email doesn't exist in our DB
    //   } else {
  
    //     //const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
    //     const isPasswordCorrect = bcrypt.compareSync(req.Password, result.Password);
  
    //     console.log(isPasswordCorrect);
  
    //     if(isPasswordCorrect){
    //       //return res.send({ access: auth.createAccessToken(result)})
    //       return { access: auth.createAccessToken(result)}
    //     } else {
    //       return false; // Passwords do not match
    //     }
    //   }
    // })
  
}



module.exports.setUserAsAdmin = async (req, res) => {

  try{
    const user = await User.findByIdAndUpdate(req.params.id, { isAdmin: true });

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }else{
      res.status(200).json({ message: "Set User as Admin successfully!" });
    }

  }catch (error) {
    console.error("Error setting user as admin:", error);
    res.status(500).json({ message: "Internal Server Error" });                   
  }

}