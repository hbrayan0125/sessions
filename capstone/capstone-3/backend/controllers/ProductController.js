const Product = require('../models/Product');
const auth = require('../auth');


module.exports.createProduct = async (req, res) => {

    if(req.user.isAdmin){
        console.log(req.file)
        if(!req.body.Name || !req.body.Description || !req.body.Price){
            return ("Your request has been rejected. Please provide all required fields.")
        }else{

            let newProduct = new Product({
                Name: req.body.Name,
                Description: req.body.Description,
                Price: req.body.Price,
                Stock: req.body.Stock                                                         
            })                  
        

            return newProduct.save().then((result, error) => {
                return (result);
            }).catch(error => {
                console.error(error); // Log the error for debugging
                return ("Internal Server Error");
            });
        }

    }else{
        res.send("you can't have access to create a product")
    }

}

module.exports.retrieveAllProduct = async (req, res) => {

    await Product.find({}).then((result) => {
        if (result.length === 0){
            return res.send(false);
        }else{
           return res.send(result)
        }
    }).catch(error => {
        console.error(error); // Log the error for debugging
        return res.status(500).send("Internal Server Error");
    });

}

module.exports.retrieveAllActiveProduct = async (req, res) => {

    await Product.find({isActive: true}).then((results) => {
        if (results.length === 0){
            return res.send(false);
        }else{
            return res.send(results)
        }
    }).catch(error => {
        console.error(error); // Log the error for debugging
        return res.status(500).send("Internal Server Error");
    });

}

module.exports.retrieveSingleProduct = async (req, res) => {

    await Product.find({_id: req.body.id}).then((results) => {
        if (results.length === 0){
            return res.send("No matching records found.");
        }else{
            return res.send(results)
        }
    }).catch(error => {
        console.error(error); // Log the error for debugging
        return res.status(500).send("Internal Server Error");
    });

}

module.exports.updateProduct = async (req, res) => {

    const updateProduct = {
        Name: req.body.Name,
        Description: req.body.Description,
        Price: req.body.Price,
        Stock: req.body.Stock
    }

    try{
        const updatedProduct = await Product.findByIdAndUpdate(req.body.id, updateProduct);

        if (!updatedProduct) {
            // No document with the provided ID was found
            return res.status(404).json({ message: "Product not found." });
        }

        res.status(200).json({message: "Product Updated successfully!"});

    }catch (error) {
        console.error(error); // Log the error for debugging
        res.status(500).json({ message: "Internal Server Error" });
    }

}

module.exports.archiveProduct = async (req, res) => {

    try{
        const updatedProduct = await Product.findByIdAndUpdate(req.body.id, {isActive: false});

        if (!updatedProduct) {
            // No document with the provided ID was found
            return res.status(404).json({ message: "Product not found." });
        }

        res.status(200).json({message: "Product Archive successfully!"});

    }catch (error) {
        console.error(error); // Log the error for debugging
        res.status(500).json({ message: "Internal Server Error" });
    }

}

module.exports.activateProduct = async (req, res) => {

    try{
        const updatedProduct = await Product.findByIdAndUpdate(req.body.id, {isActive: true});

        if (!updatedProduct) {
            // No document with the provided ID was found
            return res.status(404).json({ message: "Product not found." });
        }

        res.status(200).json({message: "Product Activate successfully!"});

    }catch (error) {
        console.error(error); // Log the error for debugging
        res.status(500).json({ message: "Internal Server Error" });
    }

}