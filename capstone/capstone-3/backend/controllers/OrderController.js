const AddtoCart = require("../models/AddtoCart");
const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");


module.exports.CreateOrder = async (req, res) => {

    if(req.user.isAdmin){
        res.send("Only Costumers are allowed!")
    }else{
        if (!req.body.id) {
            return res.status(400).send("productId is missing in req.body");
        }else{
            const productId = req.body.id;
            const totalAmount = req.body.totalPrice
            await Product.findById(productId).then(result => {
                if (!result) {
                    return res.status(404).send("Product not found");
                }else{

                    const newOrder = new Order({
                        userId: req.user.id,
                        products: [{
                            productId: productId,
                            quantity: 1
                        }],
                        SubtotalAmount: result.Price,
                        totalAmount: totalAmount
                    })

                    return newOrder.save().then(result => {
                        return res.send({message: "Purchased Successfully!"})
                    }).catch(error => {
                        console.error(error)
                        return res.status(500).send("Internal Server Error");
                    });
                }
            }).catch(error => {
                console.error(error)
                return res.status(500).send("Internal Server Error");
            }); 
        }
    }

}

module.exports.RetrieveUserDetails = async (req, res) => {
    await User.findById(req.user.id).then(result => {
        return res.send(result)
    })
}



module.exports.RetrieveUserOrder = async (req, res) => {
    try {
      const orders = await Order.find({ userId: req.user.id });
  
      if (orders.length === 0) {
        // return res.send({message: "No orders found for this user"});
        return res.send(false)
      }else{
        const UserDetails = [];
    
        for (const order of orders) {
            for (const product of order.products) {
            const result = await Product.findById(product.productId);
    
            const userDetails = {
                Email: req.user.Email,
                products: {
                productId: product.productId,
                productName: result.Name,
                productDescription: result.Description,
                productPrice: order.totalAmount,
                },
                quantity: product.quantity,
                purchasedOn: order.purchasedOn,
            };
    
            UserDetails.push(userDetails);
            }
        }
    
        return res.send(UserDetails);
    }
    } catch (error) {
      console.error("Error retrieving user details:", error);
      return res.status(500).send("Internal server error");
    }
};

module.exports.RetrieveAllOrder = async (req, res) => {

    if (req.user.isAdmin){
        await Order.find({}).then(result => {
            if (result.length > 0) {
                return res.send(result);
            } else {
                return res.send("No orders found");
            }
        }).catch(error => {
            return res.status(500).send("Internal Server Error");
        });
    }else{
        return res.send("User not Allowed")
    }
    
}

module.exports.AddedProduct = async (req, res) => {

    if(req.user.isAdmin){
        res.send("Only Costumers are allowed!")
    }else if (Object.keys(req.body).length === 0) {
        res.status(400).send("Request body is empty.");
    }else{
        const productId = req.body.productId;
        await Product.find({_id: productId}).then(result => {

            if (result.length === 0) {
                // Product not found, return an error response
                return res.send({ message: "Product not found" });
            }

            let subtotal = 0;
            for(const totalPrice of result){
                subtotal += totalPrice.Price * Number(req.body.quantity)
            }

            const newAddtoCart = new AddtoCart({
                userId: req.user.id,
                productId: productId,
                quantity: req.body.quantity,
                subtotal: subtotal,
                totalPrice: subtotal
            })

            return newAddtoCart.save().then(result => {
                return res.send({message: "Add to Cart Successfully!"})
            }).catch(error => error)
        })
    }

}

module.exports.ChangeProductQuantities = async (req, res) => {

    if(req.user.isAdmin){
        res.send("Only Costumers are allowed!")
    }else if (Object.keys(req.body).length === 0) {
        res.status(400).send("Request body is empty.");
    }else{

        await AddtoCart.findById(req.params.id).then(result => {

            if (!result) {
                // AddtoCart not found, return an error response
                return res.status(404).json({ message: "AddtoCart not found" });
            }

            Product.findById(result.productId).then(product => {

                const subtotal = product.Price * Number(req.body.quantity)

                const updateAddtoCart = {
                    quantity: req.body.quantity,
                    subtotal: subtotal,
                    totalPrice: subtotal
                }
    
                return AddtoCart.findByIdAndUpdate(req.params.id, updateAddtoCart).then((product, error) => {
                    if (error){
                    return res.send(false)
                    }else{
                    return res.send(true)
                    }
                })
            })
        })

        
    }

}

module.exports.RemoveProduct = async (req, res) => {
    if(req.user.isAdmin){
        res.send("Only Costumers are allowed!")
    }else{

        try{
            const removeProduct = await AddtoCart.findByIdAndUpdate(req.params.id, {isActive: false});

            if(!removeProduct){
                return res.status(404).json({ message: "AddtoCart not found" });
            }

            res.status(200).json({message: "Remove Product successfully!"});

        }catch{
            res.status(500).json({message: "Internal Server Error"})
        }                               
    }

}

module.exports.GetTotalPrice = async (req, res) => {
    if(req.user.isAdmin){
        res.send("Only Costumers are allowed!")
    }else{
        try {
            const Cart = await AddtoCart.find({ userId: req.user.id });
            let totalPrice = 0;

            if(Cart.length === 0){
                return res.status(404).json({ message: "AddtoCart not found" });
            }

            for (const order of Cart) {
                totalPrice += Number(order.totalPrice);
            }
            res.status(200).json({totalPrice: `${totalPrice}`});
            
        } catch (error) {
            console.error("Your Add to Cart is empty:", error);
            return res.status(500).send("Internal server error");
        }                                                             
    }
}
