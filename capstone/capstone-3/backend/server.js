const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

require("dotenv").config();

const UserRoutes = require("./routes/UserRoutes");
const ProductRoutes = require("./routes/ProductRoutes");
const OrderRoutes = require("./routes/OrderRoutes");

const port = 4000;

const app = express();

mongoose.connect(
    "mongodb+srv://admin:admin123@cluster0.2pfefby.mongodb.net/Capstone3?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,

        // allows us to avoid any current or future errors while connecting to mongodb
    }
);

// Set notification for connection success or failure
let db = mongoose.connection;

// If error occured, output in console.
db.on("error", console.error.bind(console, "Connection error!"));

// If connection is successful, output in console
db.once("open", () => console.log("Now connected to the Database!"));

app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(cors());

// All Routes Here
app.use("/users", UserRoutes);
app.use("/products", ProductRoutes);
app.use("/users/order", OrderRoutes);


if(require.main == module){
    app.listen(port, () => {
        console.log(`E-commerce Server is running port ${port}`)
    })
}

module.exports = app;