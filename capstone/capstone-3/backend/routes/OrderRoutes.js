const express = require('express');
const OrderController = require('../controllers/OrderController')
const auth = require('../auth')

const {verify, verifyAdmin} = auth;

const router = express.Router();

// Create Order
router.post('/createOrder', verify, OrderController.CreateOrder )

// Retrieve User Details
router.post('/RetrieveUserDetails', verify, OrderController.RetrieveUserDetails )


// Retrieve Authenticated User Orders
router.post('/RetrieveUserOrder', verify, OrderController.RetrieveUserOrder )

// Retrieve All Orders
router.post('/RetrieveAllOrder', verify, OrderController.RetrieveAllOrder )

// Added Product to Cart
router.post('/AddedProduct', verify, OrderController.AddedProduct )

// Change Product Quantities
router.put('/ChangeProductQuantities/:id', verify, OrderController.ChangeProductQuantities )

// Remove Product to Cart
router.put('/RemoveProduct/:id', verify, OrderController.RemoveProduct )

// Get Total Price All Product to Cart
router.get('/GetTotalPrice', verify, OrderController.GetTotalPrice )

    
module.exports = router