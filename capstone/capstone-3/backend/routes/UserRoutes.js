const express = require('express');
const userController = require('../controllers/UserController');
const auth = require('../auth.js');


const {verify, verifyAdmin} = auth;


const router = express.Router();

// User Registration
router.post('/registerUser', (req, res) => {
  userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User Authentication
router.post('/login', (req, res) => {
  userController.UserAuthentication(req.body).then(resultFromController => res.send(resultFromController));
});

// Set User as Admin
router.post('/setUserAsAdmin/:id',  userController.setUserAsAdmin);

module.exports = router