const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    userId: {
        type: String,
        require: [true, "User ID is required!"]
    },
    products: [{
        productId: {
            type: String,
            require: [true, "Product ID is required!"]
        },
        quantity: {
            type: Number,
            require: [true, "Quantity is required!"]
        }
    }],
    totalAmount: {
        type: Number,
        require: [true, "Total Amount is required!"]
    },
    SubtotalAmount: {
        type: Number,
        require: [true, "Total Amount is required!"]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Order", orderSchema)