const express = require('express');
const ProductController = require('../controllers/ProductController');
const auth = require('../auth.js');

const {verify, verifyAdmin} = auth;


const router = express.Router();



// Create Product With Image (Admin Only)
router.post('/createProduct', verify, verifyAdmin, ProductController.createProduct);

// Retrieve all products
router.get('/retrieveAllProduct', ProductController.retrieveAllProduct )

// Retrieve all active products
router.get('/retrieveAllActiveProduct', ProductController.retrieveAllActiveProduct )

// Retrieve all active products
router.post('/retrieveSingleProduct', ProductController.retrieveSingleProduct )

// Update Product Information (Admin Only)
router.post('/updateProduct', verify, verifyAdmin, ProductController.updateProduct )

// Archive Product (Admin Only)
router.post('/archiveProduct', verify, verifyAdmin, ProductController.archiveProduct )

// Activate Product (Admin Only)
router.post('/activateProduct', verify, verifyAdmin, ProductController.activateProduct )

module.exports = router