const jwt = require("jsonwebtoken");

const secret = "SecretWalangClue";

// token creation
module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        Email: user.Email,
        isAdmin: user.isAdmin
    };
    return jwt.sign(data, secret, {})
}

module.exports.verify = (req, res, next) => {
    console.log(req.headers.authorization);

    let token = req.headers.authorization;

    if(typeof token === "undefined" ){
        return res.send("Failed!, No Token");
    }else{

        token = token.slice(7, token.length);

        console.log(token);
        
        jwt.verify(token, secret, (error, decodedToken) => {
            if (error){
                return res.send({
                    auth: 
                    "Failed",
                    message: error.message
                })
            }else{
                console.log(decodedToken)

                req.user = decodedToken;

                next();
                // Will let us proceed to the next controller

            }
        })

    }
}

// Verify if user account is admin or not
module.exports.verifyAdmin = (req, res, next) => {

    if (req.user.isAdmin){
        next()
    }else{
        return res.send({
            auth: "Failed",
            message: "Action Forbidden"
        })
    }

}