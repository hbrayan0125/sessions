const mongoose = require('mongoose');

const productSchema  = new mongoose.Schema({
    Name: {
        type: String,
        require: [true, "Product Name is Required!"]
    },
    Description: {
        type: String,
        require: [true, "Product Description is Required!"]
    },
    Price: {
        type: Number,
        require: [true, "Product Price is Required!"]
    },
    Stock: {
        type: Number,
        require: [true, "Product Stock is Required!"]
    },
    // Image: {
    //     type: String,
    //     require: [true, "Product Price is Required!"]
    // },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Product", productSchema);