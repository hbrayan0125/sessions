const mongoose = require("mongoose");

const addToCartSchema = new mongoose.Schema({
    userId: {
        type: String
    },
    productId: {
        type: String
    }, 
    quantity: {
        type: Number
    },
    isActive: {
        type: Boolean,
        default: true
    },
    subtotal: {
        type: Number
    },
    totalPrice: {
        type: Number
    },
    addedAt: {
        type: Date,
        default: Date.now,
    },
})

module.exports = mongoose.model('AddToCart', addToCartSchema);