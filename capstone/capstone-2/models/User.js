const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
    Email: {
        type: String,
        require: [true, 'Email is required!']
    },
    Password: {
        type: String,
        require: [true, 'Password is required!']
    },
    Firstname: {
        type: String,
        require: [true, 'Firstname is required!']
    },
    Lastname: {
        type: String,
        require: [true, 'Lastname is required!']
    },
    isAdmin: {
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model("User", UserSchema);