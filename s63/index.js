function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    if (letter.length !== 1) {
        return undefined; // Invalid input
    }

    let count = 0;
    for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
            count++;
        }
    }

    return count;

}


    



function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

 
    const lowercaseText = text.toLowerCase();
    
    const filteredText = lowercaseText.split('').filter((letter, index, array) => {
        return array.indexOf(letter, index + 1) !== -1;
    });
    
    return filteredText.length === 0;
    
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    if (age < 13) {
        return undefined;
    }
    
    // Check if age is between 13 and 21 or age is 65 and above
    if ((age >= 13 && age <= 21) || age >= 65) {
        // Calculate a 20% discount
        let discountedPrice = price * 0.8;
        discountedPrice = discountedPrice * 100;
        // Round the discounted price
        let roundedDiscountedPrice = Math.round(discountedPrice);
        // Return the rounded discounted price as a string
        roundedDiscountedPrice = roundedDiscountedPrice / 100;
        return roundedDiscountedPrice.toString();
    }
    
    // Check if age is between 22 and 64
    if (age >= 22 && age <= 64) {
        // Return the rounded price as a string
        const noDiscount = Math.round(price * 100)/100.0;

        return noDiscount.toString();
    }
    
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    // Create an empty Set to store unique hot categories
    const hotCategories = new Set();
    
    for (const item of items) {
        if (item.stocks === 0) {
            hotCategories.add(item.category);
        }
    }
    
    return Array.from(hotCategories);

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    const flyingVoters = candidateA.filter(voter => candidateB.includes(voter));

    return flyingVoters;
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};