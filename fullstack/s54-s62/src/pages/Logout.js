import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import {useContext, useEffect} from 'react';

export default function Logout() {

    const { unsetUser, setUser } = useContext(UserContext);

    // updates localStorage to empty / clears the storage
    unsetUser();

    useEffect(()=>{
        setUser({id: null, isAdmin: null, access: null});
    })


    // Redirect back to login
    return (
        <Navigate to='/login' />
    )

}