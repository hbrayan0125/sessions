import {Button, Row, Col} from 'react-bootstrap';

export default function Banner(){
    return(
        <Row>
            <Col className="p-5 text-center">
                <h1>404 - Not Found</h1>
                <p>The page you are looking for cannot be found</p>
                <Button variant="primary">Back Home</Button>
            </Col>
        </Row>
    )
}