import { Card } from 'react-bootstrap';
import { useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function Profile(){
    const {user} = useContext(UserContext);
    return(
        (user.access === null)?
            <Navigate to="/courses" />
        :
        <>
            <Card className="mb-3 px-3 bg-primary text-light">
                <Card.Body>
                    <Card.Title className="my-5"><h1> Profile </h1></Card.Title>
                    <Card.Subtitle><h3>James Dela Cruz</h3></Card.Subtitle>
                    <hr/>
                    <Card.Subtitle className='mt-3'><h4>Contacts</h4></Card.Subtitle>
                    <ul className='my-3'>
                        <li> Email: jamesDC@gmail.com</li>
                        <li> Mobile No: 09469383656</li>
                    </ul>
                </Card.Body>
		    </Card>	
        </>
    )
}