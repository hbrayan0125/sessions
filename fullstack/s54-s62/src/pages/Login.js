import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login() {
  const {user, setUser} = useContext(UserContext);
  // State variables for email, password, and button activation
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  // Regular expression for email validation
  const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;

  // Function to handle form submission
  function authenticate(event) {
    event.preventDefault();

    // Check if the email is valid using regex
    if (!emailRegex.test(email)) {
      alert('Please enter a valid email address.');
      return;
    }

    fetch('http://localhost:4000/users/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email,
          password,
        }),
      })
        .then((res) => res.json())
        .then((data) => {

          console.log(data);

          if (typeof data.access !== "undefined") {
            
            localStorage.setItem('token', data.access);

            // function for retrieving user details
            retrieveUserDetails(data.access);

            // setUser({
            //   access: localStorage.getItem('token')
            // })

            Swal.fire({
              title: "Login Successful!",
              icon: "success",
              text: "Welcome to Zuitt!"
            });
          } else {

            Swal.fire({
              title: "Authentication Failed!",
              icon: "error",
              text: "Check your login details and try again!"
            });

          }
        });

        setEmail('');
        setPassword('');

  }

  const retrieveUserDetails = (token) => {
    fetch("http://localhost:4000/users/details", {
      headers: {
        Authorization: `Bearer ${token}`
      }

    }).then( res => res.json())
    .then(data => {
      console.log(data);

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    })
  }

  // Effect to enable/disable the button based on email and password
  useEffect(() => {
    setIsActive(email !== '' && password !== '');
  }, [email, password]);

  return (
    
    (user.id !== null)?
    <Navigate to="/courses" />
    :
    <Form onSubmit={(event) => authenticate(event)}>
      <h1 className="my-5 text-center">Login</h1>

      <Form.Group>
        <Form.Label>Email</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Email"
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </Form.Group>

      <Form.Group>
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password" 
          placeholder="Enter Password"
          required
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </Form.Group>

      {/* Use a single Button element */}
      <Button variant="primary" type="submit" className="my-3" disabled={!isActive}>
        Login
      </Button>
    </Form>
  );
}