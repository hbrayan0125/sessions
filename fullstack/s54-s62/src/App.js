import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import { Container } from 'react-bootstrap';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage';
import Profile from './pages/Profile';
// BrowserRouter, Routes, Route works together to setup components endpoints
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { useEffect, useState } from 'react';

import {UserProvider} from './UserContext';
import CourseView from './pages/CourseView';
import AdminView from './pages/AdminView';


function App() {

  // Gets the token from local storage and assigns the token to user state
  const [user, setUser] = useState({
    // token: localStorage.getItem('token')
    id: null,
    isAdmin: null,
    access: null
  });

  // unsetUser is a function for clearing local storage 
  const unsetUser = () => {
    localStorage.clear();
  }

  // Check if user info is properly stored upon login and the localStorage if cleared upon logout

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]);

  return (
    // The user state,  setUser(setter function) and unsetUser function is provided in the UserContext to be shared to other pages/components
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <Container>
          <AppNavbar />
          <Routes>
            <Route path="/" element = {<Home />} />
            <Route path="/*" element = {<ErrorPage />} />
            <Route path="/courses" element = {<Courses />} />
            <Route path="/register" element = {<Register />} />
            <Route path="/courses/:courseId" element = {<CourseView />} />
            <Route path="/adminView" element = {<AdminView />} />
            <Route path="/login" element = {<Login />} />
            <Route path="/profile" element = {<Profile />} />
            <Route path="/logout" element = {<Logout />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
    // <>
    //   <AppNavbar />
    //   <Container>
    //     <Home />
    //     <Courses />
    //     <Register />
    //     <Login />
    //   </Container>
      
    // </>
  );
}

export default App;
