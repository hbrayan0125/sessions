import { Card, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';
// import coursesData from '../data/coursesData';
// import PropTypes from 'prop-types';
import { useState } from 'react';

export default function CourseCard({courseProp}) {
	// Checks to see if the data was successfully passed
	// console.log(props);

	// // Every components recieves information in a form of an object
	// console.log(typeof props);

	const {_id, name, description, price} = courseProp;

	// Use the state hook in this component to be able to store its state

	/*
		Syntax:
			const [getter, setter] = useState(initialGetterValue);
	
	*/

	// const [count, setCount] = useState(0);
	// function enroll(){
	// 	(count === 30) ? window.alert('No more seats.') : setCount(count + 1);
	// }

	return (
		<Card className="mb-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PhP {price}</Card.Text>
				{/* <Card.Text>Enrollees: {count}</Card.Text> */}
				<Link className="btn btn-primary" to={`/courses/${_id}`} variant="primary">Details</Link>
			</Card.Body>
		</Card>	
	)
}

// Check if the CourseCard component is getting the correct prop types

// CourseCard.propTypes = {
// 	course: PropTypes.shape({
// 		name: PropTypes.string.isRequired,
// 		description: PropTypes.string.isRequired,
// 		price: PropTypes.number.isRequired
// 	})
// }