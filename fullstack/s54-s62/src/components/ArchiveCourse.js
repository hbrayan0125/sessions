import { Button } from 'react-bootstrap';
import { useState, useContext, useEffect } from 'react';
import Swal from "sweetalert2";
import CourseCard from './CourseCard';
import UserContext from '../UserContext';

export default function ArchiveCourse({course}){

    const [archive, setArchive] = useState(false);
    const [courses, setCourses] = useState([]);
    const {user} = useContext(UserContext);


    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
        .then(res => res.json())
        .then(data => {

            // Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components
            // setCourses(data);
			if(user.isAdmin){
				setCourses(data);
			}else{
				setCourses(data.map((course) => {
						return (
							<CourseCard key={course._id} courseProp={course}/>
						);
					})
				);
			}

        });
    }

    useEffect(() => {

        fetchData()

    }, []);
    

    const Archive = (courseId) => {

        fetch(`http://localhost:4000/courses/${courseId}/archive`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
            isActive: archive,
        })
        })
        .then((res) => res.json())
        .then((data) => {
            console.log(data);

            if (data) {
            Swal.fire({
                title: "Archive Success!",
                icon: "success",
                text: "Course Successfully Updated!",
            });
            // Assuming you have a state variable called 'setArchive', you can use it here
            setArchive(true);
            console.log(courses)
            } else {
            Swal.fire({
                title: "Archive Error!",
                icon: "error",
                text: "Please try again!",
            });
            }
        });

    }

    const Unarchive = (courseId) => {

        fetch(`http://localhost:4000/courses/${courseId}/activate`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
            isActive: archive,
        })
        })
        .then((res) => res.json())
        .then((data) => {
            console.log(data);

            if (data) {
            Swal.fire({
                title: "UnArchive Success!",
                icon: "success",
                text: "Course Successfully Updated!",
            });
            // Assuming you have a state variable called 'setArchive', you can use it here
            setArchive(false);
            } else {
            Swal.fire({
                title: "UnArchive Error!",
                icon: "error",
                text: "Please try again!",
            });
            }
        });

    }

    

    return (
        <>

            {
                (archive)?
                    <Button variant="success" size="sm" onClick={() => {Unarchive(course)}}>UnArchive</Button>
                :
                    <Button variant="danger" size="sm" onClick={() => {Archive(course)}}>Archive</Button>
            }
            

        </>
    )
}