import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom'
import { useContext } from 'react';
import UserContext from '../UserContext';

export default function AppNavbar(){

    const {user} = useContext(UserContext);
    // const [user, setUser] = useState(localStorage.getItem("token"));
    // console.log(user);

    // activity
    // const {Logged} = useContext(UserContext);

    return (
        <Navbar expand="lg" className="light">
            <Container>
                <Navbar.Brand href="#home">Zuitt Booking</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    {/* exact - useful for NavLinks to respond with exact path NOT partial matches  */}
                    <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
                    <Nav.Link as={NavLink} to="/courses" exact>Courses</Nav.Link>

                    {(user.id !== null) ? 
                    <>              
                        <Nav.Link as={NavLink} to="/profile" exact>Profile</Nav.Link>    
                        <Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
                    </>
                    :
                    <>
                        <Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
                        <Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
                    </>
                    }
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}