let http = require('http');

let port = 4000;

let app = http.createServer((req, res) => {

	if(req.url == "/" && req.method == "GET"){

		//
		res.writeHead(200, {'Content-type' : 'text/plain'});

		// Ends the response process
		res.end('Welcome to Booking System');
	}

	if(req.url == "/profile" && req.method == "GET"){

		//
		res.writeHead(200, {'Content-type' : 'text/plain'});

		// Ends the response process
		res.end('Welcome to your profile!');
	}

	if(req.url == "/courses" && req.method == "GET"){

		//
		res.writeHead(200, {'Content-type' : 'text/plain'});

		// Ends the response process
		res.end(`Here's our courses available`);
	}



	// The method "POST"
	// if(req.url == "/items" && req.method == "POST"){

	// 	//
	// 	res.writeHead(200, {'Content-type' : 'text/plain'});

	// 	// Ends the response process
	// 	res.end('Data to be sent to the database');
	// }


});

// First Argument, the port number assign the server
// Second Argument, the callback/function to run when the server is running

app.listen(port, () => console.log("Server is running at localhost:4000"));



//Do not modify
//Make sure to save the server in variable called app
// if(require.main === module){
//     app.listen(4000, () => console.log(`Server running at port 4000`));
// }

// module.exports = app;
