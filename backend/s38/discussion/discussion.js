let http = require('http');

let port = 4000;

let app = http.createServer((req, res) => {

	if(req.url == "/items" && req.method == "GET"){

		//
		res.writeHead(200, {'Content-type' : 'text/plain'});

		// Ends the response process
		res.end('Data retrieved from the database');
	}


	// The method "POST"
	if(req.url == "/items" && req.method == "POST"){

		//
		res.writeHead(200, {'Content-type' : 'text/plain'});

		// Ends the response process
		res.end('Data to be sent to the database');
	}


});

// First Argument, the port number assign the server
// Second Argument, the callback/function to run when the server is running

app.listen(port, () => console.log("Server is running at localhost:4000"));