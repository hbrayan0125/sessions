let http = require('http');

let port = 4000;

// Mock Database

let directory = [

	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}

];

http.createServer((req, res) => {

	if(req.url == "/users" && req.method == "GET"){

		res.writeHead(200, {'Content-type' : "application/json"});

		res.write(JSON.stringify(directory));
		res.end();

	}

	if(req.url == "/users" && req.method == "POST"){


		// Declare and initialize a "requestBody" variable to empty string
		// This will act as a placeholder for the data/resource that we do have.
		let requestBody = " ";


		// Data is received from the client and is process in the "data" stream
		req.on('data', function(data) {
			requestBody += data;
			console.log("This is from req.on(data)");
		});


		// Response end step - only runs after the request has completely been sent
		req.on('end', () => {



			requestBody = JSON.parse(requestBody);
			
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			};

			console.log(newUser);
			directory.push(newUser);

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(newUser));
			res.end();

		});

	}

}).listen(port);

console.log("Server is running at localhost:4000");

