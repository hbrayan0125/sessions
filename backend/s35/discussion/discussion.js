// [SECTION] Comparison Query Operator

// greater than / greater than or equal
// $gt / $gte

/*

	SYNTAX: 

	db.collectionName.find({field: {$gt : value}});

	db.collectionName.find({field: {$gte : value}});

*/

db.users.find({age : {$gt : 50}});
db.users.find({age : {$gte : 50}});


// less than / less than or equal
// $lt / $lte

/*

	SYNTAX: 

	db.collectionName.find({field: {$lt : value}});

	db.collectionName.find({field: {$lte : value}});

*/

db.users.find({age : {$lt : 50}});
db.users.find({age : {$lte : 50}});


// not equal
// $ne

/*

	SYNTAX: 

	db.collectionName.find({field: {$ne : value}});

*/

db.users.find({age : {$ne : 82}});


// getting the user 
// $in

/*

	SYNTAX: 

	db.collectionName.find({field: {$in : [value1, value2]}});

*/

db.users.find({lastName : {$in : ["Hawking", "Doe"]}});

db.users.find({courses : {$in : ["Html", "React"]}});


// [SECTION] Logical Query Operator
// $or

/*

	SYNTAX: 

	db.collectionName.find({$or: [{fieldA: value},{fieldB: value}]}});

*/


db.users.find({$or: [{firstName: "Neil"},{age: 25}]});

// or with gt
db.users.find({$or: [{firstName: "Neil"},{age: {$gt : 25}}]});


// $and operator

/*

	db.collectionName.find({$and: [{fieldA: value},{fieldB: value}]}});

*/

db.users.find({$and: [{age: {$ne: 82}},{age: {$ne: 76}}]});

// [SECTION] Field Projection

// Inclusion

/*
    - Allows us to include/add specific fields only when retrieving documents.
    - The value provided is 1 to denote that the field is being included.
    - Syntax
        db.users.find({criteria},{field: 1})
*/

db.users.find(
	{firstName: "Jane"}
	,
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);


// Exclusion

db.users.find(
	{firstName: "Jane"}
	,
	{
		contact: 0,
		department:  0
	}
);


// Suppressing the ID Field

db.users.find(
	{firstName: "Jane"},
	{

		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}

);

// [SECTION] Evaluation Query Operator

// $regex operator

/*

	SYNTAX: 

	db.users.find({field: {$regex: "pattern", $option: "$optionvalue"}});

*/

// Case sensitive Query

db.users.find({firstName: {$regex: "N"}});

// Case insensitive Query

// MongoDB v4 /i -> $i

db.users.find({firstName: {$regex: "j", $options: "i"}});

db.users.find({firstName: {$regex: "n", $options: "i"}});


db.users.find(
	{firstName: "Jane"}
	,
	{
		firstName: 1,
		lastName: 1,
		"contact.email": 1
	}
);


