console.log("Hello World")

//[ SECTION ] Syntax, Statements and Comments
// JS Statement usually end with semicolon (;)

// Comments:
// There are two types of comments;
// 1. The single-line comment denoted by //, double slash
// 2. the multi-line comment denoted by /**/, double and asterisk in the middle

// [ SECTION ] Variables
	// It is used to contain data
	// Any information that is used by an application is 
	// stored in what we call a "memory" //


	// Declaring Variables

	// Declaring Variables - tells our devices that a variable name is created.
	 
	let myVariableNumber; // A naming convension

	console.log(myVariableNumber)
	// let MyVariableNumber;
	// let my-variable-number;
	// let my_variable_number;

	// Syntax
		// let/const/var

	//Declaring and Initializing Variables

	let productName = "desktop computer"
	console.log(productName)

	let productPrice1 = "18999"
	let productPrice = 18999
	console.log(productPrice1)
	console.log(productPrice)

	const interest = 3.539

	// Reassigning variable values

	productName = "Laptop"
	console.log(productName)

	// interest = 4.239 // will return an error

	// console.log(interest)

	// Reassigning Variables vs Initializing Variables

	// Declare the variable

	myVariableNumber = 2023
	console.log(myVariableNumber)

	// Multiple Variable Declarations

	let productCode = 'DC017'
	let productBrand = 'Dell'

	console.log(productCode, productBrand)

	// [ SECTION ] Data Types

		// Strings 

		let country = "Philippines"
		let province = "Metro Manila"

		// Concatenation Strings
		let fullAddress = province + ", " + country
		console.log(fullAddress)

		let greeting = "I live in the " + country
		console.log(greeting)

		let mailAddress = 'Metro Manila \n\nPhilippines'

		console.log(mailAddress)

		let message = "John's employees went home early"
		console.log(message)

		message = 'John\'s employees went home early again!'
		console.log(message)

		// Numbers
		let headcount = 26
		console.log(headcount)

		// Decimal Numbers/Fractions
		let grade = 98.7
		console.log(grade)

		// Exponential Notation
		let planetDistance = 2e10
		console.log(planetDistance)

		// Combining text and strings
		console.log("John's grade last quarter is " + grade)

		// Boolean 
		// Returns true or false

		let isMarried = false
		let isGoodConduct = true
		console.log("isMarried: " + isMarried)
		console.log("inGoodConduct: " + isGoodConduct)

		// Arrays
		let grades = [ 98.7, 92.1, 90.2, 94.6 ]
		console.log(grades);

		let details = ["John", "Smith", 32, true]
		console.log(details)
		console.log(details[0])
		console.log(details[1])
		console.log(details[2])
		console.log(details[3])

		//Objects
		//Composed of "key/value pair"


		let person = {
			fullName: 'Juan Dela Cruz',
			age: 35,
			isMarried: false,
			contact: ["09123456789", "09987654321"],
			address: {
				houseNumber: "345",
				city: "Manila"
			}

		}


		console.log(person)

		

