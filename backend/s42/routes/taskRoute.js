const express = require("express");
const router = express.Router();


const taskController = require("../controllers/taskController.js");

// Get all tasks
router.get("/", (req, res) => {
	taskController.getAllTask().then(resultFromController => res.send(resultFromController));
});

router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Delete a task using wildcard on params
// ":" -> wildcard
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});

module.exports = router;