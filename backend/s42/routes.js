const express = require("express");
const router = express.Router();


const taskController = require("../controllers/taskController.js");

// Get all tasks
router.get("/", (req, res) => {
	taskController.getAllTask();
});

module.exports = router;