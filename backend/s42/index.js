const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute.js")

const app = express();
const port = 4000;



// MongoDB Connection using Mongoose

mongoose.connect("mongodb+srv://admin:admin123@cluster0.2pfefby.mongodb.net/B305-to-do?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true

	// allows us to avoid any current or future errors while connecting to mongodb
});

// Set notification for connection success or failure
let db = mongoose.connection;

// If error occured, output in console.
db.on("error", console.error.bind(console, "Connection error!"))

// If connection is successful, output in console
db.once("open", () => console.log("We're connected to the cloud database."));



// Middlewares
// Allows our app to read json datatype
// Allows to read and accept data in form
app.use(express.json());
app.use(express.urlencoded({extended: true}));
// default endpoint
// localhost:4000/	
app.use("/tasks", taskRoute);


// port listening
if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`));
}

module.exports = app;