// 1. What directive is used by Node.js in loading the modules it needs? 

// answer: Node.js uses the require directive to load the modules it needs.



// 2. What Node.js module contains a method for server creation?

// answer: The Node.js module that contains a method for server creation is 'http'.



// 3. What is the method of the http object responsible for creating a server using Node.js?

// answer: The method responsible for creating a server using Node.js is 'http.createServer()'.


// 4. What method of the response object allows us to set status codes and content types?

// answer: The method of the response object that allows us to set status codes and content types is 'response.writeHead()'.
    

// 5. Where will console.log() output its contents when run in Node.js?

// answer: 'console.log()' will output its contents to the standard output, which is typically the terminal or console where you are running the Node.js application.



// 6. What property of the request object contains the address's endpoint?

// answer: The property of the request object that contains the address's endpoint is 'request.url'.
