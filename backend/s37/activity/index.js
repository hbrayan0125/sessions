//Add solution here

let http = require("http");

let port = 3000

http.createServer(function(req, res){

	if(req.url == "/login"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Welcome to the login page");
	}else if(req.url == "/register"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("I'm sorry the page you are looking for cannot be found");
	}else{
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("404: Page not found");
	}

}).listen(port);

// Output will be shown in the terminal
console.log(`Server is running at localhost:${port}`);






















//Do not modify
module.exports = {app}