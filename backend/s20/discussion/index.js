// console.log("Hello World")

// [SECTION] Arithmetic Operators


let x = 50;
let y = 10;
let sum = x + y;
console.log("Result from addition Operator: " + sum);

let difference = x - y;
console.log("Result from subtraction Operator: " + difference);

let product = x * y;
console.log("Result from multiplication Operator: " + product);

let quotient = x / y;
console.log("Result from division Operator: " + quotient);

// modulo %

let remainder = x % y;
console.log("Result from modulo Operator: " + remainder);


// [SECTION] Assignment Operators
// Basic Assignment Operator (=)

let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("addition assignment: " + assignmentNumber)

// Shorthand Method for Assignment Method

assignmentNumber += 2;
console.log("addition assignment: " + assignmentNumber)

assignmentNumber -= 2;
console.log("subtraction assignment: " + assignmentNumber)

assignmentNumber *= 2;
console.log("multiplication assignment: " + assignmentNumber)

assignmentNumber /= 2;
console.log("division assignment: " + assignmentNumber)

//Multiple Operators and Parentheses

let mdas = 1+2-3*4/5;
console.log("Result from mdas operation: " + mdas);

let pemdas = 1 + (2 -3) * (4 / 5);
console.log("Result from pemdas operation: " + pemdas);

// Increment and Decrement
// Incrementation -> ++
// Decrementation -> --

let z = 1;

// pre-increment
let increment = ++z;
console.log("Pre-increment: " + increment)
console.log("Pre-increment: " + z);

//post-increment
increment = z++;
console.log("post-increment: " + z);

let myNum1 = 1;
let myNum2 = 1;

let preIncrement = ++myNum1;
preIncrement = ++myNum1;
console.log(preIncrement);

let postIncrement = myNum2++;
postIncrement = myNum2++;
console.log(postIncrement);

let myNumA = 5;
let myNumB = 5;

let preDecrement = --myNumA;
preDecrement = --myNumA;
console.log(preDecrement);

let postDecrement = myNumA--;
postDecrement = myNumA--;
console.log(postDecrement);

// [SECTION] Type Coercion
// Automatic or implicit conversion of value type to another

let myNum3 = "10";
let myNum4 = 12;

let coercion = myNum3 + myNum4;
console.log(coercion);
console.log(typeof coercion);

let myNum5 = true + 1;
console.log(myNum5);

let myNum6 = false + 1;
console.log(myNum6);

// [SECTION] Comparison Operator

let juan = "juan";

// Equality Operator (==)
console.log(juan == "juan");
console.log(1 == 2);
console.log(false == 0);


// Inequality Operator (!=)
// ! -> NOT
console.log(juan != "juan");
console.log(1 != 2);
console.log(false != 0);

// Strict Equality Operator (===)
// Same data type, content or value
console.log(1 === 1);
console.log(false === 0);

// Strict Inequality (!==)
console.log(1 !== 1);
console.log(false !== 0);

// [SECTION] Relational Operator
// comparison operators whether one value is greater or less than to the value.\

let a = 50;
let b =  65;

let example1 = a > b;
console.log(example1);

let example2 = a < b;
console.log(example2);

let example3 = a >= b;
console.log(example3);

let example4 = a <= b;
console.log(example4);


// [SECTION] Logical Operator

// Logical "AND" (&&)

let isLegalAge = true;
let isRegistered = true;
let allRequirementsMet = isLegalAge && isRegistered;

console.log("Logical AND: " + allRequirementsMet);


// Logical "OR" (||)
let isLegalAge2 = true;
let isRegistered2 = false;
let someRequirementsMet = isLegalAge2 || isRegistered2;

console.log("Logical OR: " + someRequirementsMet);

// Logical "NOT" (!)
let isRegistered3 = false;
let someRequirementsNotMet = !isRegistered3;

console.log("Logical NOT: " + someRequirementsNotMet);




