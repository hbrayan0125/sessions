// [ SECTION ] Dependencies and Modules
const express = require('express');
const courseController = require('../controllers/courseController');
const auth = require('../auth.js');

// Destructure from auth

const {verify, verifyAdmin} = auth;

// [ SECTION ] Routing Component
const router = express.Router();

// course registration routes
router.post('/', verify, verifyAdmin, courseController.addCourse);

// get all courses
router.get('/all', courseController.getAllCourses);

// get all "active" course
router.get('/', courseController.getAllActive);

// get 1 specific course using its ID
router.get('/:courseId', courseController.getCourse);

// Updating a Course (Admin Only)
router.put('/:courseId', verify, verifyAdmin, courseController.updateCourse)

// Archive a Course (Admin Only)
router.put('/:courseId/archive', verify, verifyAdmin, courseController.archiveCourse)

// Activate a Course (Admin Only)
router.put('/:courseId/activate', verify, verifyAdmin, courseController.activateCourse)


// [ SECTION ] Export Route System
module.exports = router;