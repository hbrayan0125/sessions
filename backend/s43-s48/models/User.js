// [SECTION] Dependecies and Modules
const mongoose = require("mongoose");

// [SECTION] Schema/Blueprint
const UserSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [ true, 'First Name is required!' ]
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is required!']
	},
	email: {
		type: String,
		required: [true, 'Email is required!']
	},
	password: {
		type: String,
		required: [true, 'Password is required!']
	},
	isAdmin: {
		type: Boolean,
	 	default: false
	},
	mobileNo: {
		type: String,
		required: [true, 'mobileNo is required!']
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, 'courseId is required!']
			},
			EnrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]
})

// [SECTION] Model
module.exports = mongoose.model('User', UserSchema);

