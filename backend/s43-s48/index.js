// [SECTION] Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

require("dotenv").config();

//Allows access to routes defined within our application
const userRoutes = require('./routes/user');
const courseRoutes = require('./routes/course');

// [SECTION] Environment Setup
const port = 4000;

// [SECTION] Server Setup
const app = express();

// MongoDB Connection using Mongoose

mongoose.connect(
  "mongodb+srv://admin:admin123@cluster0.2pfefby.mongodb.net/S43-S48?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,

    // allows us to avoid any current or future errors while connecting to mongodb
  }
);

// Set notification for connection success or failure
let db = mongoose.connection;

// If error occured, output in console.
db.on("error", console.error.bind(console, "Connection error!"));

// If connection is successful, output in console
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors()); // Allows all resources to access our backend application
// [ SECTION ] Backend Routes
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// [SECTION] Server Gateway Response
if (require.main === module) {
  app.listen(process.env.PORT || port, () =>
    console.log(`API is now online on port ${process.env.PORT || port}`)
  );
}

module.exports = app;
