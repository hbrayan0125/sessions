const express = require("express");

const app = express();
const port = 4000;

// Middlewares
app.use(express.json());

// Allow your app to read data from forms

app.use(express.urlencoded({extended: true}));
// accepts all datatype

// [SECTION] Routes


app.get("/", (req, res) => {
	res.send("Hello World!");
})

app.get("/hello", (req, res) => {
	res.send("Hello from /hello end point");
})

// route expects to receive a POST Method at the URI "/hello"
app.post("/hello", (req, res) => {
	res.send(`Hello there, ${req.body.firstName} ${req.body.lastName}!`);
})

// POST route to register user
let users = [];

let defaultuser = {
	username: "johndoe",
	password: "johndoe123"
}

users.push(defaultuser);

app.post("/signup", (req, res) => {
	console.log(req.body);

	for(let i = 0; i < users.length; i++){
		if(users[i].username === req.body.username && users[i].password === req.body.password){
			res.send("The user is already exist")
		}else if(req.body.username !== "" && req.body.password !== ""){
			users.push(req.body);
			res.send(`User ${req.body.username} successfully registered! `)
		}else{
			res.send("Please input both username and password")
		}
	}
})

app.put("/change-password", (req, res) => {
	let message;

	let currentusers = JSON.stringify(users);

		for(let i=0; i < users.length; i++){

				if (req.body.username == users[i].username){
					users[i].password = req.body.password;
				message = `User ${req.body.username}'s password has been updated.`;
					break;
				}else{

					message = "User does not exist."
				}
			}

	res.send(message);
})

if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`));
}

