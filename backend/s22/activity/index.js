//we can use this 3.1416*(radius**2) for circle


/*
    
    1. Create a function called addNum which will be able to add two numbers.
        - Numbers must be provided as arguments.
        - Return the result of the addition.
       
       Create a function called subNum which will be able to subtract two numbers.
        - Numbers must be provided as arguments.
        - Return the result of subtraction.

        Create a new variable called sum.
         - This sum variable should be able to receive and store the result of addNum function.

        Create a new variable called difference.
         - This difference variable should be able to receive and store the result of subNum function.

        Log the value of sum variable in the console.
        Log the value of difference variable in the console

 */

function addNum(addend1, addend2) {
    return addend1 + addend2;
}

const addend1 = parseFloat(prompt("Enter the first addend:"));
const addend2 = parseFloat(prompt("Enter the  second addend:"));

const sum = addNum(addend1, addend2)

console.log("Displayed Sum of " + addend1 + " and " + addend2);
console.log(sum);
// Difference
function subNum(minuend, subtrahend) {
    return minuend - subtrahend;
}

const minuend = parseFloat(prompt("Enter the minuend:"));
const subtrahend = parseFloat(prompt("Enter the subtrahend:"));

const difference = subNum(minuend, subtrahend)

console.log("Displayed Difference of " + minuend + " and " + subtrahend);
console.log(difference);
 
 /*       
    2. Create a function called multiplyNum which will be able to multiply two numbers.
        - Numbers must be provided as arguments.
        - Return the result of the multiplication.

        Create a function divideNum which will be able to divide two numbers.
        - Numbers must be provided as arguments.
        - Return the result of the division.

        Create a new variable called product.
         - This product variable should be able to receive and store the result of multiplyNum function.

        Create a new variable called quotient.
         - This quotient variable should be able to receive and store the result of divideNum function.

        Log the value of product variable in the console.
        Log the value of quotient variable in the console.

*/

function multiplyNum(factor1, factor2) {
    return factor1 * factor2;
}

const factor1 = parseFloat(prompt("Enter the first factor:"));
const factor2 = parseFloat(prompt("Enter the second factor:"));

const product = multiplyNum(factor1, factor2)

console.log("Displayed Product of " + factor1 + " and " + factor2);
console.log(product);
// Quotient
function divideNum(dividend, divisor) {
    return dividend / divisor;
}

const dividend = parseFloat(prompt("Enter the dividend:"));
const divisor = parseFloat(prompt("Enter the divisor:"));

const quotient = divideNum(dividend, divisor)

console.log("Displayed Quotient of " + dividend + " and " + divisor);
console.log(quotient);

/*
    3. Create a function called getCircleArea which will be able to get total area of a circle from a provided radius.
        - a number should be provided as an argument.
        - look up the formula for calculating the area of a circle with a provided/given radius.
        - look up the use of the exponent operator.
        - return the result of the area calculation.

        Create a new variable called circleArea.
        - This variable should be able to receive and store the result of the circle area calculation.
        - Log the value of the circleArea variable in the console.


*/

function getCircleArea(circleArea){
    const area =  Math.PI * (circleArea ** 2);
    return area.toFixed(2);
}

const circleArea = parseFloat(prompt("Please input the radius: "));
console.log("The result of getting the area of a circle with " + circleArea + " radius: \n" + getCircleArea(circleArea));



/*
    4. Create a function called getAverage which will be able to get total average of four numbers.
        - 4 numbers should be provided as an argument.
        - look up the formula for calculating the average of numbers.
        - return the result of the average calculation.
        
        Create a new variable called averageVar.
        - This variable should be able to receive and store the result of the average calculation
        - Log the value of the averageVar variable in the console.

*/

function getAverage(num1, num2, num3, num4)
{
    const totalsum = num1 + num2 + num3 + num4;
    return totalsum / 4;
}

const input = prompt("Enter four numbers separated by Comma :");
const [input1, input2, input3, input4] = input.split(",").map(parseFloat);
const averageVar = getAverage(input1, input2, input3, input4);

console.log("The average of " +  input1 + "," + input2 + "," + input3 + "," + input4 + ": \n" + averageVar);  


/*

    5. Create a function called checkIfPassed which will be able to check if you passed by checking the percentage of your score against the passing percentage.
        - this function should take 2 numbers as an argument, your score and the total score.
        - First, get the percentage of your score against the total. You can look up the formula to get percentage.
        - Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
        - return the value of the variable isPassed.
        - This function should return a boolean.

        Create a global variable called outside of the function called isPassingScore.
            -This variable should be able to receive and store the boolean result of the checker function.
            -Log the value of the isPassingScore variable in the console.
*/

function checkIfPassed(score, totalscore){
    const percentage = ((score / totalscore) * 100)
    let isPassed;

    if (percentage >= 75)
    {
        return isPassed = true;
    } else {
        return isPassed = false;
    } 
}

const AskScore = prompt("Input the your score then slash (/) lastly input the Total Score of your test");
const [score, totalscore] = AskScore.split("/").map(parseFloat);
const isPassingScore = checkIfPassed(score, totalscore);

console.log("Is " + score + "/" + totalscore + " a passing score? \n" + isPassingScore);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        addNum: typeof addNum !== 'undefined' ? addNum : null,
        subNum: typeof subNum !== 'undefined' ? subNum : null,
        multiplyNum: typeof multiplyNum !== 'undefined' ? multiplyNum : null,
        divideNum: typeof divideNum !== 'undefined' ? divideNum : null,
        getCircleArea: typeof getCircleArea !== 'undefined' ? getCircleArea : null,
        getAverage: typeof getAverage !== 'undefined' ? getAverage : null,
        checkIfPassed: typeof checkIfPassed !== 'undefined' ? checkIfPassed : null,

    }
} catch(err){

}