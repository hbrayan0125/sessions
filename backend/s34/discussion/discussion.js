// CRUD Operations (MongoDB)

// CRUD Operations are the heart of any backend app.

// Mastering CRUD Operations is essential for any developers

/*

 C - Create/Insert
 R - Retrieve/Read
 U - Update
 D - Delete

*/

// [SECTION] Creating Documents

/*

	SYNTAX:

	db.users.insertOne({object})

	INSERT INTO table_name(column1, column2, column3, ...) VALUES (value1, value2, value3, ...);


*/

db.users.insertOne({
	firstName: "Brayan",
	lastName: "Hernandez",
	age: 16,
	contact: {
		phone: "09120898332",
		email: "brayanhernandez@gmail.com"
	},
	courses: ["CSS", "JS", "Python"],
	department: "none"
});


/*

	SYNTAX:

	db.users.insertOneMany([{object}])

	INSERT INTO table_name(column1, column2, column3, ...) VALUES (value1, value2, value3, ...);


*/

db.users.insertMany([
{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "09123456789",
		email: "stephenhawking@gmail.com"
	},
	courses: ["Php", "React", "Python"],
	department: "none"
},
{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "09123456789",
		email: "neilarmstrong@gmail.com"
	},
	courses: ["React", "Laravel", "Sass"],
	department: "none"
}
]);

// [SECTION] Read/Retrieve

/*

	db.usersfindOne();
	db.usersfindOne({field: value});
	
*/

db.users.findOne();
db.users.findOne({firstName: "Stephen"});


// [SECTION] find Many 

/*

	db.usersfind();
	db.usersfind({field: value});
	
*/

db.users.find({department: "none"});

// [SECTION] Multiple Criteria

db.users.find({department: "none", age: 82});


// [SECTION] Updating

/*

	SYNTAX: 

	db.collectionName.updateOne({criteria}, {$set: {field: value}})

*/

//current
db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

//new
db.users.updateOne(
{firstName: "Test"},
 {
 	$set: {
 		firstName: "Bill",
 		lastName: "Gate",
 		age 65,
 		contact: {
 			phone: "123456789",
 			email: "billgates@gmail.com"
 		},
 		courses: ["Php", "Laravel", "Html"],
 		department: "Operations",
 		status: "active"
 	}
 }
);

// new 1
db.users.updateOne(
{firstName: "Test"},
{
	$set: {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "123456789",
			email: "billgates@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	}
}
);

// find the data

db.users.findOne({"firstName": "Bill"});

db.users.findOne({"contact.email": "billgates@gmail.com"});

/*

	Update many Collection

	db.users.updateMany({criteria}, {$set: {field: value}})

*/

db.users.updateMany(
{department: "none"},
{
	$set: {
		department: "HR"
	}
}
);


// [SECTION] Deleting a data

db.users.insert({
	firstName: "Test"
});

// Delete single document

/*

	db.users.deleteOne({firstName: "Test"});

*/

db.users.deleteOne({firstName: "Test"});

// Delete Many


// Insert another Bill
db.users.insert({
	firstName: "Bill"
});

db.users.deleteMany({firstName: "Bill"});
